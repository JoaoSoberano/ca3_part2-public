package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("Public constructor null first name - throw exception")
    void publicConstructorNullFirstName() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee(null, "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        });
    }

    @Test
    @DisplayName("Public constructor empty first name - throw exception")
    void publicConstructorEmptyFirstName() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        });
    }

    @Test
    @DisplayName("Public constructor null last name - throw exception")
    void publicConstructorNullLastName() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("Frodo", null, "ring bearer", "doctor", "111111@gmail.com");
        });
    }

    @Test
    @DisplayName("Public constructor empty last name - throw exception")
    void publicConstructorEmptyLastName() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("Frodo", "", "ring bearer", "doctor", "111111@gmail.com");
        });
    }


    @Test
    @DisplayName("Public constructor null description - throw exception")
    void publicConstructorNullDescription() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("Frodo", "Baggins", null, "doctor", "111111@gmail.com");
        });
    }

    @Test
    @DisplayName("Public constructor empty description - throw exception")
    void publicConstructorEmptyLastDescription() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("Frodo", "Baggins", "", "doctor", "111111@gmail.com");
        });
    }

    @Test
    @DisplayName("Public constructor null job title - throw exception")
    void publicConstructorNullJobTitle() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", null, "111111@gmail.com");
        });
    }

    @Test
    @DisplayName("Public constructor empty job title - throw exception")
    void publicConstructorEmptyLastJobTitle() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "", "111111@gmail.com");
        });
    }

    @Test
    @DisplayName("Public constructor null email - throw exception")
    void publicConstructorNullEmail() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", null, null);
        });
    }

    @Test
    @DisplayName("Public constructor empty email - throw exception")
    void publicConstructorEmptyLastJobEmail() {
        //arrange
        //assert
        assertThrows(Exception.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "", "");
        });
    }


    @Test
    @DisplayName("Return true when two object are equals - same object")
    void testEqualsSameObject() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        //act
        boolean result = employee.equals(employee);
        //assert
        assertTrue(result);
    }

    @Test
    @DisplayName("Return true when two object are equals - same values")
    void testEqualsSameValues() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        Employee employee1 = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        //act
        boolean result = employee.equals(employee1);
        //assert
        assertTrue(result);
    }

    @Test
    @DisplayName("Return false when two objects are different - different class")
    void testNotEqualsDifferentClass() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        String string = new String("name");
        //act
        boolean result = employee.equals(string);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return false when two objects are different -null")
    void testNotEqualsNull() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        //act
        boolean result = employee.equals(null);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return false when two objects are different - first name")
    void testNotEqualsDifferentFirstName() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        Employee employee1 = new Employee("John", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        //act
        boolean result = employee.equals(employee1);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return false when two objects are different - last name")
    void testNotEqualsDifferentLastName() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        Employee employee1 = new Employee("Frodo", "Garcia", "ring bearer", "doctor", "111111@gmail.com");
        //act
        boolean result = employee.equals(employee1);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return false when two objects are different - description")
    void testNotEqualsDifferentDescription() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        Employee employee1 = new Employee("Frodo", "Baggins", "bearer", "doctor", "111111@gmail.com");
        //act
        boolean result = employee.equals(employee1);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return false when two objects are different - job title")
    void testNotEqualsDifferentJobTitle() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        Employee employee1 = new Employee("Frodo", "Baggins", "ring bearer", "engineer", "111111@gmail.com");
        //act
        boolean result = employee.equals(employee1);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return false when two objects are different - email")
    void testNotEqualsDifferentEmail() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        Employee employee1 = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "2222222@gmail.com");
        //act
        boolean result = employee.equals(employee1);
        //assert
        assertFalse(result);
    }

    @Test
    @DisplayName("Return true when hashcode's are equal - same object")
    void testHashEqualsSameObject() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        //assert
        assertTrue(employee.hashCode() == employee.hashCode());
    }

    @Test
    @DisplayName("Return true when hashcode's are equal - different object")
    public void testHashEqualsDifferentObjects() {
        //arrange
        Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        Employee employee1 = new Employee("John", "Baggins", "ring bearer", "doctor", "111111@gmail.com");
        //assert
        assertFalse(employee.hashCode() == employee1.hashCode());
    }

    @Test
    @DisplayName("Ensure i can get the first name")
    void getFirstName() {
        //arrange
        String expected = "Frodo";
        Employee employee = new Employee(expected, "Baggings", "description", "engineer", "11111@hotmail.com");
        //act
        String result = employee.getFirstName();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the first name for the expected value - throws exception because is null")
    void setFirstNameNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setFirstName(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the first name for the expected value - throws exception because is empty")
    void setFirstNameEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setFirstName("");
        });

    }

    @Test
    @DisplayName("Ensure i can get the last name")
    void getLastName() {
        //arrange
        String expected = "Baggings";
        Employee employee = new Employee("Frodo", expected, "description", "engineer", "11111@hotmail.com");
        //act
        String result = employee.getLastName();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the last name for the expected value - throws exception because is null")
    void setLastNameNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setLastName(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the last name for the expected value - throws exception because is empty")
    void setLastNameEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setLastName("");
        });

    }

    @Test
    @DisplayName("Ensure i can get the last name")
    void getDescription() {
        //arrange
        String expected = "description";
        Employee employee = new Employee("Frodo", "Baggings", expected, "engineer", "11111@hotmail.com");
        //act
        String result = employee.getDescription();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the description for the expected value - throws exception because is null")
    void setDescriptionNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setDescription(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the description for the expected value - throws exception because is empty")
    void setDescriptionEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setDescription("");
        });
    }

    @Test
    @DisplayName("Ensure i can get the job title")
    void getJobTitle() {
        //arrange
        String expected = "engineer";
        Employee employee = new Employee("Frodo", "Baggings", "description", expected, "11111@hotmail.com");
        //act
        String result = employee.getJobTitle();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the job title for the expected value - throws exception because is null")
    void setJobTitleNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setJobTitle(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the job title for the expected value - throws exception because is empty")
    void setJobTitleEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setJobTitle("");
        });
    }

    @Test
    @DisplayName("Ensure i can get the email")
    void getEmail() {
        //arrange
        String expected = "11111@hotmail.com";
        Employee employee = new Employee("Frodo", "Baggings", "description", "engineer", expected);
        //act
        String result = employee.getEmail();
        //assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Ensure i can not set the email for the expected value - throws exception because is null")
    void setEmailNull() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setEmail(null);
        });
    }

    @Test
    @DisplayName("Ensure i can not set the email for the expected value - throws exception because is empty")
    void setEmailEmpty() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setEmail("");
        });
    }

    @Test
    @DisplayName("Ensure i can not set the email - throws exception because has not the @")
    void setEmailField() {
        //arrange
        Employee emp = new Employee();
        //act + assert
        assertThrows(Exception.class, () -> {
            emp.setEmail("email");
        });
    }

    @Test
    @DisplayName("Public constructor - throws Exception with email with no @")
    void emailThrowsExceptionConstructor() {
        //arrange + act + assert
        assertThrows(Exception.class, () -> {
            new Employee("Ana", "Marcelino", "description", "nurse", "email");
        });
    }




}